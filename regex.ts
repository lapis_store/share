const jsonDate =
    /'^\d+-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2]\d)|(3[0-1]))T(([0-1]\d)|(2[0-4]))(:([0-5]\d)){2}.\d{3}Z$'/i;

const regex = {
    jsonDate,
};

export default regex;
