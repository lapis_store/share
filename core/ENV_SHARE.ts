class ENV_SHARE {
    public static readonly API_HOST = 'http://service-lapisstore.lapisblog.com';
    public static readonly STATIC_HOST: string = 'http://static-lapisstore.lapisblog.com';
    public static readonly WEB_NAME = 'Lapis Store';
}

export default ENV_SHARE;
