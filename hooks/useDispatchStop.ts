const useDispatchStop = <P>(callbackFunc: (props: P) => void, time: number = 700) => {
    let counter: number = 0;
    let isRunning = false;

    // let timerId:NodeJS.Timer|undefined = undefined;
    let preValue: any = null;
    let newValue: any = null;

    return (props: P): void => {
        newValue = props;

        if (isRunning) return;

        isRunning = true;
        counter = 0;

        const timerId = setInterval(() => {
            if (counter >= 9) {
                clearInterval(timerId);
                counter = 0;
                isRunning = false;

                callbackFunc(newValue);
            }

            if (preValue === newValue) {
                counter += 1;
                return;
            }

            counter = 0;
            preValue = newValue;
        }, Math.round(time / 10));
    };
};

export default useDispatchStop;
