import * as React from 'react';
import make from '../../functions/make';

import styles from './QuantityInput.module.scss';

export interface IQuantityInputProps {
    className?: string;
    max?: number;
    min?: number;
    defaultValue?: number;

    onChange?: (v: number) => any;
    onButtonNextClick?: () => any;
    onButtonPrevClick?: () => any;
}

export default function QuantityInput(props: IQuantityInputProps) {
    // console.log('[QuantityInput] Rerender');

    const { onChange } = props;
    const [inputValue, setInputValue] = React.useState<string>('');

    const handlerInputChange = (e: React.FormEvent<HTMLInputElement>) => {
        setInputValue(e.currentTarget.value);
    };

    const handlerInputBlur = () => {
        if (!onChange) return;

        const numberValue = parseInt(inputValue);
        if (!isFinite(numberValue)) {
            setInputValue(() => {
                return String(props.min || 0);
            });

            onChange(props.min || 0);
            return;
        }

        let currentValue = numberValue;

        if (props.min !== undefined && numberValue < props.min) {
            currentValue = props.min;
        } else if (props.max !== undefined && numberValue > props.max) {
            currentValue = props.max;
        }

        setInputValue(String(currentValue));
        onChange(currentValue);
    };

    const handlerButtonClick = (v: 1 | -1) => () => {
        if (!onChange) return;

        const numberValue = parseInt(inputValue);
        if (!isFinite(numberValue)) {
            setInputValue(() => {
                return String(props.min || 0);
            });

            onChange(props.min || 0);
            return;
        }

        let currentValue = numberValue + v;
        if (props.min !== undefined && currentValue < props.min) {
            return;
        }
        if (props.max !== undefined && currentValue > props.max) {
            return;
        }

        onChange(currentValue);
    };

    React.useEffect(() => {
        // console.log(props.defaultValue);
        setInputValue(() => {
            if (typeof props.defaultValue !== 'number' || !isFinite(props.defaultValue)) {
                return String(props.min || 0);
            }
            return String(props.defaultValue);
        });
    }, [props.defaultValue, props.min]);

    return (
        <div className={make.className([styles['quantity-input'], props.className])}>
            <div className={styles['buttons']}>
                <button onClick={handlerButtonClick(-1)}>arrow_left</button>
                <button onClick={handlerButtonClick(1)}>arrow_right</button>
            </div>
            <div className={styles['input-container']}>
                <input
                    type='number'
                    min={props.min}
                    max={props.max}
                    value={inputValue}
                    onChange={handlerInputChange}
                    onBlur={handlerInputBlur}
                />
            </div>
        </div>
    );
}
