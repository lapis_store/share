import LapisBaseSelection, { IOption } from './LapisBaseSelection';

export type { IOption };
export default LapisBaseSelection;
