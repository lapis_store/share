import * as React from 'react';
import make from '../../functions/make';
import makeClassName from '../../functions/make/className';
import LapisCover from '../LapisCover';

import styles from './LapisBaseSelection.module.scss';

export interface IOption {
    label: string;
    value: any;
}

export interface ILapisBaseSelectionProps {
    className?: string;
    options?: IOption[];
    selected?: IOption;
    selectedValue?: any;

    onSelected?: (v: IOption) => any;
}

export default function LapisBaseSelection(props: ILapisBaseSelectionProps) {
    const [isExpand, setIsExpand] = React.useState<boolean>(false);

    const selectedLabel: string = (() => {
        if (props.selected) return props.selected.label;
        if (!props.selectedValue) return '';
        if (!props.options) return '';

        const selectedItem = props.options.find((item) => item.value === props.selectedValue);
        if (!selectedItem) return '';
        return selectedItem.label;
    })();

    const handlerLabelClick = () => {
        setIsExpand((v) => !v);
    };

    const handlerCoverClick = () => {
        setIsExpand(false);
    };

    const handlerOptionClick = (v: IOption) => () => {
        if (props.onSelected) props.onSelected(v);
        setIsExpand(false);
    };

    const liElmnts = props.options?.map((item, i) => {
        return (
            <li key={i} onClick={handlerOptionClick(item)}>
                {item.label}
            </li>
        );
    });

    return (
        <div className={make.className([styles['lapis-base-selection'], props.className])}>
            <LapisCover display={isExpand} zIndex={19} onClick={handlerCoverClick} />

            <div
                className={makeClassName(['wrap', isExpand && 'expand'], styles)}
                style={
                    {
                        '--number-of-item': props.options?.length || 0,
                    } as any
                }
            >
                <label onClick={handlerLabelClick}>{selectedLabel}</label>
                <ul>{liElmnts}</ul>
            </div>
        </div>
    );
}
