import * as React from 'react';
import make from '../../functions/make';
import LapisCover from '../LapisCover';

import styles from './MessageBox.module.scss';

export interface IMessageBox {
    id?: string;
    title?: string;
    message?: string;
    icon?: string;
    backgroundColor?: string;
    buttons?: {
        icon?: string;
        label: string;
        callback?: () => any;
    }[];
}

export interface IMessageBoxProps extends IMessageBox {
    className?: string;
    onClose?: (id?: string) => any;
    index?: number;
}

export default function MessageBox(props: IMessageBoxProps) {
    const [isCovering, setCoverState] = React.useState<boolean>(true);

    const handlerCoverClick = () => {
        setCoverState(false);
        if (props.onClose) props.onClose(props.id);
    };

    const handlerButtonClick = (callback?: () => any) => () => {
        if (callback) callback();
        if (props.onClose) props.onClose(props.id);
    };

    const renderButtonsElmnts = () => {
        if (!props.buttons || props.buttons.length === 0) return <button>Ok</button>;

        return props.buttons.map((item, i) => {
            return (
                <button key={`${item.label}_${i}`} onClick={handlerButtonClick(item.callback)}>
                    {item.label}
                </button>
            );
        });
    };

    const margin = `${(props.index || 0) * 15}px`;

    return (
        <div
            className={make.className([styles['message-box'], props.className])}
            style={
                {
                    '--background-color': props.backgroundColor,
                    '--plus': margin,
                } as any
            }
        >
            <div className={styles['window']}>
                <div className={styles['header']}>
                    <strong>{props.title || 'Thông báo'}</strong>
                    <button onClick={handlerCoverClick}>close</button>
                </div>
                <div className={styles['body']}>
                    <div className={styles['icon']}>{props.icon || 'sms'}</div>
                    <div className={styles['message']}>{props.message}</div>
                </div>
                <div className={styles['footer']}>{renderButtonsElmnts()}</div>
            </div>

            <LapisCover className={styles['cover']} display={isCovering} onClick={handlerCoverClick} />
        </div>
    );
}
