import * as React from 'react';

import './LapisGlobalComponents.scss';

export interface ILapisGlobalComponentsProps {}

export default function LapisGlobalComponents(props: React.PropsWithChildren<ILapisGlobalComponentsProps>) {
    return <div className='lapis-global-components'>{props.children}</div>;
}
