import React from 'react';
import make from '../../functions/make';
import LapisCover from '../LapisCover';
import InputWrap from './InputWrap';

import styles from './LapisSearch.module.scss';
import Suggestion from './Suggestion';
import { ISuggestion } from './Suggestion/Suggestion';

export interface ILapisSearchProps {
    className?: string;
    suggestions?: ISuggestion[];
    suggestionTitle?: string;

    onChange?: (e: React.FormEvent<HTMLInputElement>) => any;
    onFocus?: () => any;
    onBlur?: () => any;
    onGetSuggestions?: (v: string) => Promise<ISuggestion[] | undefined>;
    onSearch?: (v: string) => any;
}

export default function LapisSearch(props: ILapisSearchProps) {
    const { onFocus, onBlur, onGetSuggestions, onSearch } = props;
    const [displayOffer, setDisplayOffer] = React.useState<boolean>(false);
    const [suggestions, setSuggestions] = React.useState<ISuggestion[] | undefined>(undefined);

    // Event handler
    const handlerInputFocus = React.useCallback(() => {
        setDisplayOffer(true);
        if (onFocus) onFocus();
    }, [onFocus]);

    const handlerClickOutSide = React.useCallback(() => {
        setDisplayOffer(false);
        if (onBlur) onBlur();
    }, [onBlur]);

    const handlerTypingStop = React.useCallback(
        async (v: string) => {
            if (!onGetSuggestions) return;
            setSuggestions(await onGetSuggestions(v));
        },
        [onGetSuggestions],
    );

    // const handlerSearch = React.useCallback((v:string)=>{
    //     if(onSearch) onSearch(v);
    //     // handlerClickOutSide();
    //     // document.getElementsByTagName('body')[0].focus();
    // }, [onSearch, handlerClickOutSide]);

    return (
        <div
            className={make.className([
                styles['lapis-search'],
                props.className,
                displayOffer && styles['display-suggestion'],
            ])}
        >
            <div className={styles['wrap']}>
                <InputWrap
                    displayOffer={displayOffer}
                    onFocus={handlerInputFocus}
                    onChange={props.onChange}
                    onTypingStop={handlerTypingStop}
                    onSearch={onSearch}
                />
                <Suggestion display={displayOffer} suggestions={suggestions} title={props.suggestionTitle} />
            </div>
            <LapisCover className={styles['cover']} onMouseDown={handlerClickOutSide} display={displayOffer} />
        </div>
    );
}
