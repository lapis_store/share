import * as React from 'react';
import make from '../../../functions/make';
import useDispatchStop from '../../../hooks/useDispatchStop';

import styles from './InputWrap.module.scss';

export interface IInputWrapProps {
    displayOffer?: boolean;

    onFocus?: () => any;
    onBlur?: () => any;
    onButtonSearchClick?: () => any;
    onChange?: (e: React.FormEvent<HTMLInputElement>) => any;

    onTypingStop?: (v: string) => any;

    onSearch?: (v: string) => any;
}

export default function InputWrap(props: IInputWrapProps) {
    const { onSearch } = props;

    const [value, setValue] = React.useState<string>('');

    const onTypingStopRef = React.useRef(props.onTypingStop);
    onTypingStopRef.current = props.onTypingStop;

    const dispatchValueRef = React.useRef(
        useDispatchStop((v: string) => {
            if (onTypingStopRef.current) {
                onTypingStopRef.current(v);
            }
        }, 500),
    );

    const handlerInputChange = React.useCallback((e: React.FormEvent<HTMLInputElement>) => {
        const v = e.currentTarget.value;
        dispatchValueRef.current(v);
        setValue(v);
    }, []);

    const handlerInputKeydown = React.useCallback(
        (e: React.KeyboardEvent<HTMLInputElement>) => {
            switch (e.key) {
                case 'Enter': {
                    const v = e.currentTarget.value;
                    if (onSearch) onSearch(v);
                    return;
                }
            }
        },
        [onSearch],
    );

    const handlerButtonSearchClick = () => {
        if (onSearch) onSearch(value);
    };

    return (
        <div className={make.className([styles['input-wrap'], props.displayOffer && styles['display-offer']])}>
            <input
                className={styles['input']}
                type='text'
                autoComplete='off'
                value={value}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                onChange={handlerInputChange}
                onKeyDown={handlerInputKeydown}
            />
            <button className={styles['button']} type='button' onClick={handlerButtonSearchClick} />
        </div>
    );
}
