import * as React from 'react';
import LapisLink from '../../../../flex/LapisLink';
import make from '../../../functions/make';

import styles from './Suggestion.module.scss';

export interface ISuggestion {
    href: string;
    text: string;
}

export interface ISuggestionProps {
    display?: boolean;
    suggestions?: ISuggestion[];
    title?: string;
}

export default function Suggestion(props: ISuggestionProps) {
    const suggestionsElmnts = props.suggestions?.map((item, i) => {
        return (
            <li key={`${item.href}_${i}`}>
                <div>
                    <LapisLink href={item.href} className={styles['a']}>
                        {item.text}
                    </LapisLink>
                </div>
            </li>
        );
    });

    return (
        <div className={make.className([styles['suggestion'], props.display && styles['display']])}>
            <div className={styles['line']}>
                <div></div>
            </div>
            <div className={styles['title']}>
                <div>{props.title}</div>
            </div>
            <ul>{suggestionsElmnts}</ul>
        </div>
    );
}
