import LapisSearch from './LapisSearch';
import { ISuggestion } from './Suggestion/Suggestion';

export type { ISuggestion };
export default LapisSearch;
