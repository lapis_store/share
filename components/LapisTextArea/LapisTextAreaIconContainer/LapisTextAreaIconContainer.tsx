import * as React from 'react';

export interface ILapisTextAreaIconContainerProps {}

export default function LapisTextAreaIconContainer(props: ILapisTextAreaIconContainerProps) {
    return <div className='lapis-text-area-icon-container'></div>;
}
