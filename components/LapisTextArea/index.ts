import LapisTextArea, { ILapisTextAreaRef } from './LapisTextArea';

export type { ILapisTextAreaRef };
export default LapisTextArea;
