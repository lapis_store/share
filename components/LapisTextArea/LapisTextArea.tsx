import * as React from 'react';
import './LapisTextArea.scss';

export type TLapisTextAreaKeyEvent = React.KeyboardEvent<HTMLTextAreaElement>;
export type TLapisTextAreaEvent = React.FormEvent<HTMLTextAreaElement>;

export interface ILapisTextAreaProps {
    className?: string;
    type?: 'number' | 'text' | 'date' | 'datetime-local';
    value?: string;
    message?: string;
    icon?: 'verified' | 'new_releases' | string;
    iconColor?: string;
    showIcon?: boolean;
    title?: string;
    min?: string | number;
    max?: string | number;
    minLength?: number;
    validator?: RegExp;
    placeholder?: string;

    onChange?: (e: TLapisTextAreaEvent) => any;
    onBlur?: (v: string) => void;
    onFocus?: (v: string) => void;
    onEnter?: (v: string) => void;
    onKeydown?: (v: string) => void;
    onKeyUp?: (e: TLapisTextAreaKeyEvent) => any | Promise<any>;
}

export interface ILapisTextAreaRef {
    val: (v: string | undefined) => string | undefined | void;
}

function LapisTextArea(props: ILapisTextAreaProps, ref: React.RefObject<ILapisTextAreaRef> & any) {
    const { onBlur } = props;

    const [typingClassName, setTypingClassName] = React.useState<' typing' | ''>('');
    const [notEmptyClassName, setNotEmptyClassName] = React.useState<' not-empty' | ''>('');

    const textAreaRef = React.useRef<HTMLTextAreaElement>(null);

    const inputValue = React.useCallback((v: string | undefined = undefined): string | undefined | void => {
        if (!textAreaRef.current) return undefined;

        // get
        if (v === undefined) return textAreaRef.current.value;

        //set
        textAreaRef.current.value = v;

        // update state
        if (v.length === 0) setNotEmptyClassName('');
        else setNotEmptyClassName(' not-empty');
    }, []);

    React.useImperativeHandle(
        ref,
        () =>
            ({
                val: (v: string | undefined = undefined) => {
                    return inputValue(v);
                },
            } as ILapisTextAreaRef),
    );

    const handlerTextAreaFocus = React.useCallback(() => {
        setTypingClassName(' typing');
    }, []);

    const handlerTextAreaBlur = React.useCallback(() => {
        setTypingClassName('');
        const v: string = inputValue() || '';

        if (v.length === 0) setNotEmptyClassName('');
        else setNotEmptyClassName(' not-empty');

        // dispatch event
        if (onBlur) onBlur(inputValue() || '');
    }, [inputValue, onBlur]);

    return (
        <div className={'lapis-ui lapis-text-area' + notEmptyClassName + typingClassName}>
            <div className='title'>
                <div>{props.title}</div>
            </div>
            <div className='input-wrap'>
                <textarea
                    ref={textAreaRef}
                    onFocus={handlerTextAreaFocus}
                    onBlur={handlerTextAreaBlur}
                    onKeyUp={props.onKeyUp}
                    onChange={props.onChange}
                />
            </div>
            <div className='underline'>
                <div />
            </div>
        </div>
    );
}

export default React.memo(React.forwardRef<ILapisTextAreaRef, ILapisTextAreaProps>(LapisTextArea));
