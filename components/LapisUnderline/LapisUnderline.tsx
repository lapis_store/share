import * as React from 'react';
import make from '../../functions/make';

import styles from './LapisUnderline.module.scss';

export interface ILapisUnderlineProps {
    className?: string;
    isActivating: boolean;
}

function LapisUnderline(props: ILapisUnderlineProps) {
    return (
        <div className={make.className([styles['lapis-underline'], props.className])}>
            <div />
        </div>
    );
}

export default React.memo(LapisUnderline);
