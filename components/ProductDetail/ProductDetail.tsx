import * as React from 'react';
import make from '../../functions/make';
import Buttons from './components/Buttons';
import Price from './components/Price/Price';
import ProductInfo from './components/ProductInfo';
import PromotionInfo from './components/PromotionInfo';
import Title from './components/Title';

import styles from './ProductDetail.module.scss';

export interface IProductDetailProps {
    className?: string;

    title?: string;
    price?: number;
    isPromotion?: boolean;
    promotionPrice?: number;
    information?: string;
    promotion?: string;

    quantityItemInCart?: number;

    onButtonAddToCartClick?: () => any;
    onQuantityChange?: (v: number) => any;
    onButtonBuyNowButtonClick?: () => any;
}

export default function ProductDetail(props: IProductDetailProps) {
    return (
        <div className={make.className([styles['product-detail'], props.className])}>
            <Title value={props.title} />
            <Price price={props.price} isPromotion={props.isPromotion} promotionalPrice={props.promotionPrice} />
            <ProductInfo value={props.information} />
            <PromotionInfo value={props.promotion} />
            <Buttons
                onButtonAddToCartClick={props.onButtonAddToCartClick}
                onButtonBuyNowClick={props.onButtonBuyNowButtonClick}
                onQuantityChange={props.onQuantityChange}
                quantity={props.quantityItemInCart}
            />
        </div>
    );
}
