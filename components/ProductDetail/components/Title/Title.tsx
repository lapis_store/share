import * as React from 'react';

import styles from './Title.module.scss';

export interface ITitleProps {
    value?: string;
}

function Title(props: ITitleProps) {
    return (
        <div className={styles['title']}>
            <h1>{props.value}</h1>
        </div>
    );
}

export default Title;
