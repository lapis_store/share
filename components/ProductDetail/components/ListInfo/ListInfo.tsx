import * as React from 'react';
import make from '../../../../functions/make';

import styles from './ListInfo.module.scss';

export interface IListInfoProps {
    className?: string;
    value: string;
}

export default function ListInfo(props: IListInfoProps) {
    // if(!props.value) return null;

    const listInfoElmnt = props.value
        .split('\n')
        .map((item) => item.trim())
        .filter((item) => item.length > 0)
        .map((item, i) => {
            const tmp = item.split(':');
            if (tmp.length === 2) {
                return (
                    <li key={i}>
                        <strong>{tmp[0].trim()}: </strong>
                        <span>{tmp[1].trim()}</span>
                    </li>
                );
            }

            return <li key={i}>{item}</li>;
        });

    return <ul className={make.className([styles['list-info'], props.className])}>{listInfoElmnt}</ul>;
}
