import * as React from 'react';

import styles from './Price.module.scss';

export interface IPriceProps {
    isPromotion?: boolean;
    price?: number;
    promotionalPrice?: number;
}

function Price(props: IPriceProps) {
    if (typeof props.price != 'number' || !isFinite(props.price))
        return <div className={styles['price']}>Giá bị lỗi trong quá trình hiển thị.</div>;

    if (!props.isPromotion || typeof props.promotionalPrice != 'number' || !isFinite(props.promotionalPrice))
        return (
            <div className={styles['price']}>
                <strong>{props.price.toLocaleString('vi-VN')}</strong>
            </div>
        );

    const discountPercent = Math.round(100 - (props.promotionalPrice / props.price) * 100);

    return (
        <div className={styles['price']}>
            <strong>{props.promotionalPrice.toLocaleString('vi-VN')}</strong>
            <span>{props.price.toLocaleString('vi-VN')}</span>
            <i>{discountPercent}</i>
        </div>
    );
}

export default React.memo(Price);
