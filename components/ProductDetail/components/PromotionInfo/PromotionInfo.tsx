import * as React from 'react';
import make from '../../../../functions/make';
import ListInfo from '../ListInfo';

import styles from './PromotionInfo.module.scss';

export interface IPromotionInfoProps {
    className?: string;
    value?: string;
}

function PromotionInfo(props: IPromotionInfoProps) {
    if (!props.value) return null;

    return (
        <div className={make.className([styles['promotion-info'], props.className])}>
            <h2>Khuyến mãi</h2>
            <ListInfo className={styles['list']} value={props.value} />
        </div>
    );
}

export default React.memo(PromotionInfo);
