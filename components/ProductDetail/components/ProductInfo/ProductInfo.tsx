import * as React from 'react';
import make from '../../../../functions/make';
import ListInfo from '../ListInfo';

import styles from './ProductInfo.module.scss';

export interface IProductInfoProps {
    className?: string;
    value?: string;
}

function ProductInfo(props: IProductInfoProps) {
    if (!props.value) return null;

    return (
        <div className={make.className([styles['product-info'], props.className])}>
            <h2>Thông tin sản phẩm</h2>
            <ListInfo className={styles['list']} value={props.value} />
        </div>
    );
}

export default React.memo(ProductInfo);
