import * as React from 'react';
import make from '../../../../functions/make';
import QuantityInput from '../../../QuantityInput';

import styles from './Buttons.module.scss';

export interface IButtonsProps {
    className?: string;

    quantity?: number;

    onQuantityChange?: (v: number) => any;
    onButtonAddToCartClick?: () => any;
    onButtonBuyNowClick?: () => any;
}

export default function Buttons(props: IButtonsProps) {
    const hasBeenAddedToCart = (props.quantity || 0) > 0;

    return (
        <div className={make.className([styles['buttons'], props.className])}>
            <div className={make.className([styles['quantity'], hasBeenAddedToCart && styles['display']])}>
                <label>Chọn số lượng </label>
                <QuantityInput defaultValue={props.quantity || 0} min={1} onChange={props.onQuantityChange} />
            </div>
            <button
                className={make.className([styles['add-to-cart'], hasBeenAddedToCart || styles['display']])}
                onClick={props.onButtonAddToCartClick}
            >
                Thêm vào giỏ hàng
            </button>
            <button className={styles['buy-now']} onClick={props.onButtonBuyNowClick}>
                Mua ngay
            </button>
        </div>
    );
}
