import * as React from 'react';
import make from '../../../functions/make';

import styles from './Heading.module.scss';

export interface IHeadingProps {
    value: string;
}

export const headingReg = /^##h([1-4]) (.+)$/i;

export default function Heading(props: IHeadingProps) {
    const value = props.value;
    const matchResult = value.match(headingReg);

    if (!matchResult) return null;

    const [_, headingNumber, data] = matchResult;

    const className = make.className([styles['heading']]);

    switch (headingNumber) {
        case '1': {
            return <h3 className={className}>{data}</h3>;
        }
        case '2': {
            return <h4 className={className}>{data}</h4>;
        }
        case '3': {
            return <h5 className={className}>{data}</h5>;
        }
        default: {
            return <h6 className={className}>{data}</h6>;
        }
    }
}
