import * as React from 'react';
import LapisLink from '../../../../../flex/LapisLink';

export interface IBoldProps {
    v: string;
}

export const boldReg = /^([abi])`(.+)$/i;

export default function Bold(props: IBoldProps) {
    const matchResult = props.v.match(boldReg);

    if (!matchResult) return <span> props.v</span>;

    const [_, tag, value] = matchResult;

    switch (tag) {
        case 'b':
            return <b>{value}</b>;
        case 'i':
            return <i>{value}</i>;
        case 'a': {
            const tmp = value.split('`');
            if (tmp.length === 2) return <LapisLink href={`/${tmp[0]}`}>{tmp[1]}</LapisLink>;
            return <span>{value}</span>;
        }
    }

    return <span> props.v</span>;
}
