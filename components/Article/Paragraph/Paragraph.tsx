import * as React from 'react';
import Bold, { boldReg } from './inline/Bold';

export interface IParagraphProps {
    value: string;
}

export default function Paragraph(props: IParagraphProps) {
    const items = props.value.split('```');
    if (items.length === 1) {
        return <p>{props.value}</p>;
    }

    const elmnts = items.map((item, i) => {
        const key = `${i}_${item}`;
        if (boldReg.test(item)) return <Bold key={key} v={item} />;
        return <span key={key}>{item}</span>;
    });

    return <p>{elmnts}</p>;
}
