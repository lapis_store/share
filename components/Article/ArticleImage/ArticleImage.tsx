//* eslint-disable @next/next/no-img-element */
import * as React from 'react';
import make from '../../../functions/make';

import styles from './ArticleImage.module.scss';

export const articleImageReg = /^##img (.+) ;; (.*)*$/i;

export interface IArticleImageProps {
    value: string;
}

export default function ArticleImage(props: IArticleImageProps) {
    const matchResult = props.value.match(articleImageReg);

    if (!matchResult) return null;

    const [_, src, alt] = matchResult;

    return (
        <figure className={make.className([styles['picture']])}>
            <picture>
                <source media='(min-width:1000px)' srcSet={make.imageAddress(src, 'large')} />
                <source media='(min-width:600px)' srcSet={make.imageAddress(src, 'medium')} />
                <img src={make.imageAddress(src, 'small')} alt={alt} />
            </picture>
            <figcaption className={styles['description']}>{alt}</figcaption>
        </figure>
    );
}
