import * as React from 'react';
import make from '../../functions/make';

import styles from './Article.module.scss';
import ArticleImage, { articleImageReg } from './ArticleImage';
import Heading, { headingReg } from './Heading';
import Paragraph from './Paragraph/Paragraph';

export interface IArticleProps {
    className?: string;
    data?: string;
}

export default function Article(props: IArticleProps) {
    const data = props.data;

    const elmnts = React.useMemo(() => {
        if (!data) return undefined;

        return data
            .split('\n')
            .map((line) => line.trim())
            .filter((line) => {
                if (line.length === 0) return false;
                if (line.startsWith('//')) return false;
                return true;
            })
            .map((line) => {
                if (headingReg.test(line)) {
                    return <Heading key={line} value={line} />;
                }
                if (articleImageReg.test(line)) {
                    return <ArticleImage key={line} value={line} />;
                }
                return <Paragraph key={line} value={line} />;
            });
    }, [data]);

    return <article className={make.className([styles['article'], props.className])}>{elmnts}</article>;
}
