import * as React from 'react';
import make from '../../functions/make';

import styles from './LapisBaseButton.module.scss';

export interface ILapisBaseButtonProps {
    display?: boolean;
    size?: string;
    icon?: string;
    color?: string;
    helperText?: string;

    onClick?: () => any;
}

export default function LapisBaseButton(props: ILapisBaseButtonProps) {
    return (
        <button
            className={make.className(['lapis-base-button', props.display && 'display'], styles)}
            type='button'
            style={
                {
                    '--size': props.size,
                } as any
            }
            onClick={props.onClick}
        >
            <div
                className={styles['icon']}
                style={{
                    color: props.color,
                }}
            >
                {props.icon}
            </div>
            <div className={make.className(['helper-text', props.helperText && 'displayable'], styles)}>
                <div>{props.helperText}</div>
            </div>
        </button>
    );
}
