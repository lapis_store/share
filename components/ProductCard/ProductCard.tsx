///* eslint-disable @next/next/no-img-element */
import * as React from 'react';
import LapisLink from '../../../flex/LapisLink';
import make from '../../functions/make';
import TLapisReactElements from '../../types/TLapisReactElements';

import defaultImage from './default-image.png';
import styles from './ProductCard.module.scss';

export interface IProductCardProps {
    className?: string;
    slug?: string;
    image?: string;
    title?: string;
    isPromotionalPrice?: boolean;
    price?: number;
    promotionPrice?: number;
    summary?: string;
    didAddedToCart?: boolean;
    quantityAdded?: number;

    onAddToCartButtonClick?: () => any;
    onBuyNowButtonClick?: () => any;
    onDecreaseQuantityButtonClick?: () => any;
    onIncreaseQuantityButtonClick?: () => any;
    onRemoveClick?: () => any;
}

export default function ProductCard(props: IProductCardProps) {
    const { image, summary, promotionPrice, isPromotionalPrice } = props;
    const title: string = props.title || '';
    const price: number = props.price || 0;

    const formatPrice = (n: number): string => {
        return n.toLocaleString('vi-VN');
    };

    // renders
    const renderDiscountPercent = (): TLapisReactElements => {
        if (promotionPrice === undefined || !isPromotionalPrice) return undefined;

        // avoid divided by 0
        if (!price) return undefined;

        const discountPercent = Math.round((1 - promotionPrice / price) * 100);

        if (discountPercent === 0) return undefined;

        const label = discountPercent < 0 ? 'Tăng' : 'Giảm';
        return (
            <div className={styles['discount-percent']}>
                <strong>{`${label} ${Math.abs(discountPercent)}%`}</strong>
            </div>
        );
    };

    const renderImage = (): TLapisReactElements => {
        if (!image) {
            return (
                <div className={styles['image']}>
                    <img src={defaultImage.src} alt={title} />
                </div>
            );
        }
        let src = image;

        const regEx = /^http(s)?:\/\//g;

        // check if src start with 'http://' || 'https://'
        if (!regEx.test(src)) {
            src = make.imageAddress(src, 'thumbnail');
        }

        return (
            <div className={styles['image']}>
                <img src={src} alt={title} />
            </div>
        );
    };

    const renderPrice = (): TLapisReactElements => {
        if (promotionPrice === undefined || !isPromotionalPrice || promotionPrice === price) {
            return (
                <div className={styles['price']}>
                    <strong>{formatPrice(price)}</strong>
                </div>
            );
        }

        return (
            <div className={styles['price']}>
                <strong>{formatPrice(promotionPrice)}</strong>
                <span>{formatPrice(price)}</span>
            </div>
        );
    };

    const renderSummary = (): TLapisReactElements => {
        if (!summary) return undefined;

        const lines = summary.split('\n');
        return lines.map((line, i) => {
            const tmp = line.split(':');
            if (tmp.length === 2) {
                return (
                    <p key={`${i}_${line}`}>
                        <strong>{tmp[0]}: </strong>
                        <span>{tmp[1]}</span>
                    </p>
                );
            }
            return <p key={`${i}_${line}`}>{line}</p>;
        });
    };

    return (
        <div className={make.className([styles['product-card'], props.className])}>
            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>
                    <button
                        className={make.className([styles['button-remove'], props.didAddedToCart && styles['display']])}
                        onClick={props.onRemoveClick}
                    />
                </div>
            </div>

            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>{renderDiscountPercent()}</div>
            </div>

            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>{renderImage()}</div>
            </div>

            <div className={styles['layer-wrap']}>
                <div className={make.className([styles['layer'], styles['product-info-container']])}>
                    <div className={styles['product-info']}>
                        <div className={styles['title']}>
                            <LapisLink href={props.slug || '#'}>{title}</LapisLink>
                        </div>
                        {renderPrice()}
                        <div className={styles['summary']}>{renderSummary()}</div>
                        <div className={styles['buttons']}>
                            <button
                                className={make.className([
                                    styles['btn-buy-now'],
                                    props.didAddedToCart && styles['added-to-cart'],
                                ])}
                                onClick={props.onBuyNowButtonClick}
                            >
                                Mua ngay
                            </button>

                            {props.didAddedToCart ? (
                                <div className={styles['input-quantity']}>
                                    <div className={styles['buttons']}>
                                        <button className={'decrease'} onClick={props.onDecreaseQuantityButtonClick}>
                                            arrow_left
                                        </button>
                                        <button className={'increase'} onClick={props.onIncreaseQuantityButtonClick}>
                                            arrow_right
                                        </button>
                                    </div>
                                    <div className={styles['quantity']}>{props.quantityAdded}</div>
                                </div>
                            ) : (
                                <button className={styles['btn-add-to-cart']} onClick={props.onAddToCartButtonClick}>
                                    add_shopping_cart
                                </button>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
