import ProductCard, { IProductCardProps } from './ProductCard';

export type { IProductCardProps };
export default ProductCard;
