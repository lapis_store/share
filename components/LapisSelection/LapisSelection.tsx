import * as React from 'react';
import make from '../../functions/make';
import { IOption } from '../../types/IOption';
import LapisBaseButton from '../LapisBaseButton';
import LapisCover from '../LapisCover';
import HelperIcon from './components/HelperIcon';
import Input from './components/Input';
import Options from './components/Options';
import Title from './components/Title';
import Underline from './components/Underline';

import styles from './LapisSelection.module.scss';

export interface ILapisSelectionProps {
    className?: string;

    options?: IOption[];

    title?: string;
    selectedValue?: any;
    placeholder?: string;

    // icon
    displayIcon?: boolean;
    helperIcon?: string;
    helperIconColor?: string;
    helperText?: string;

    // button
    displayButton?: boolean;
    buttonIcon?: string;
    buttonIconColor?: string;
    buttonTooltip?: string;

    onChange?: (v: IOption) => any;
    onBlur?: (v: string) => any;
    onFocus?: (v: string) => any;
    onButtonClick?: () => any;
}

export const LapisSelectionContext = React.createContext<{
    readonly focus: boolean;
    readonly setFocus: React.Dispatch<React.SetStateAction<boolean>>;
    readonly options: IOption[];
    readonly inputValue: string;
    readonly setInputValue: React.Dispatch<React.SetStateAction<string>>;
    readonly selectedValue: any;
    readonly selectedLabel: string;
    readonly close: () => void;
}>({} as any);

export default function LapisSelection(props: ILapisSelectionProps) {
    const { onChange } = props;

    const options = React.useMemo(() => {
        return props.options || [];
    }, [props.options]);

    const selectedLabel: string = React.useMemo(() => {
        const selectedOption = options.find((option) => option.value === props.selectedValue);
        if (!selectedOption) return '';
        return selectedOption.label;
    }, [options, props.selectedValue]);

    const [focus, setFocus] = React.useState<boolean>(false);
    const [inputValue, setInputValue] = React.useState<string>('');

    const handlerButtonRemoveSelectedClick = () => {
        if (onChange)
            onChange({
                label: '',
                value: undefined,
            });
    };

    const close = React.useCallback(() => {
        setFocus(false);
        setInputValue('');
    }, []);

    return (
        <div className={make.className([styles['lapis-selection'], focus && styles['focus'], props.className])}>
            <LapisCover className={styles['cover']} display={focus} onClick={close} />
            <LapisSelectionContext.Provider
                value={{
                    focus,
                    setFocus,
                    options,
                    inputValue,
                    setInputValue,
                    selectedValue: props.selectedValue,
                    selectedLabel,
                    close,
                }}
            >
                <div className={styles['wrap']}>
                    <Title value={props.title} />
                    <div className={styles['input-wrap']}>
                        <Input />
                        <LapisBaseButton
                            display={props.selectedValue !== undefined}
                            icon='remove_done'
                            color='var(--title-text-color)'
                            helperText='Bỏ lựa chọn'
                            size='var(--input-height)'
                            onClick={handlerButtonRemoveSelectedClick}
                        />
                        <HelperIcon
                            display={props.displayIcon}
                            icon={props.helperIcon}
                            color={props.helperIconColor}
                            helperText={props.helperText}
                        />
                    </div>
                    <Underline />
                    <Options onChange={onChange} />
                </div>
            </LapisSelectionContext.Provider>
        </div>
    );
}
