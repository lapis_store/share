import * as React from 'react';
import make from '../../../../functions/make';
import { LapisSelectionContext } from '../../LapisSelection';

import styles from './Input.module.scss';

export interface IInputProps {}

export default function Input(props: IInputProps) {
    const { focus, inputValue, setInputValue, setFocus, selectedLabel } = React.useContext(LapisSelectionContext);

    return (
        <div className={styles['input']}>
            <div className={styles['layer']}>
                <label className={make.className([focus && 'focus'], styles)}>{selectedLabel}</label>
            </div>
            <div className={styles['layer']}>
                <input
                    type={'text'}
                    value={inputValue}
                    onFocus={() => {
                        setFocus(true);
                    }}
                    onChange={(e: React.FormEvent<HTMLInputElement>) => {
                        setInputValue(e.currentTarget.value);
                    }}
                />
            </div>
        </div>
    );
}
