import * as React from 'react';
import make from '../../../../functions/make';

import styles from './HelperIcon.module.scss';

export interface IHelperIconProps {
    display?: boolean;
    helperText?: string;
    icon?: string;
    color?: string;
}

export default function HelperIcon(props: IHelperIconProps) {
    return (
        <div className={make.className(['helper-icon', props.display && 'display'], styles)}>
            <div
                className={styles['icon']}
                style={{
                    color: props.color,
                }}
            >
                {props.icon}
            </div>

            <div className={make.className(['helper-text', props.helperText && 'displayable'], styles)}>
                <div>{props.helperText}</div>
            </div>
        </div>
    );
}
