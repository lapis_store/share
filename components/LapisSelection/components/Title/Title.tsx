import * as React from 'react';
import make from '../../../../functions/make';
import { LapisSelectionContext } from '../../LapisSelection';

import styles from './Title.module.scss';

export interface ITitleProps {
    value?: string;
}

export default function Title(props: ITitleProps) {
    const { focus, selectedValue } = React.useContext(LapisSelectionContext);

    return (
        <div className={styles['title']}>
            <div className={make.className([(focus || selectedValue !== undefined) && 'focus'], styles)}>
                {props.value}
            </div>
        </div>
    );
}
