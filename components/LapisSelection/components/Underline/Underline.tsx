import * as React from 'react';
import make from '../../../../functions/make';
import { LapisSelectionContext } from '../../LapisSelection';

import styles from './Underline.module.scss';

export interface IUnderlineProps {}

export default function Underline(props: IUnderlineProps) {
    const { focus, selectedValue } = React.useContext(LapisSelectionContext);
    return (
        <div className={styles['underline']}>
            <div className={make.className([(focus || selectedValue !== undefined) && 'focus'], styles)}></div>
        </div>
    );
}
