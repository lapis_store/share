import * as React from 'react';
import make from '../../../../functions/make';
import { LapisSelectionContext } from '../../LapisSelection';

import optimizeKeyword from '../../../../functions/optimizeKeyword';

import styles from './Options.module.scss';
import { IOption } from '../../../../types/IOption';

export interface IOptionsProps {
    onChange?: (v: IOption) => any;
}

export default function Options(props: IOptionsProps) {
    const { onChange } = props;

    const { focus, options, inputValue, selectedValue, close } = React.useContext(LapisSelectionContext);

    const deferredKeyword = React.useDeferredValue(optimizeKeyword(inputValue));

    const handlerClick = React.useCallback(
        (v: IOption) => () => {
            if (onChange) onChange(v);
            close();
        },
        [onChange, close],
    );

    const optionsElmnt = React.useMemo(() => {
        const regex = new RegExp(`${deferredKeyword}`, 'i');

        return options
            .filter((option) => {
                const label = optimizeKeyword(option.label);
                return regex.test(label);
            })
            .map((item, i) => {
                return (
                    <li
                        key={String(item.value) + i}
                        className={make.className([selectedValue === item.value && 'selected'], styles)}
                        onClick={handlerClick({
                            label: item.label,
                            value: item.value,
                        })}
                    >
                        {item.label}
                    </li>
                );
            });
    }, [options, deferredKeyword, handlerClick, selectedValue]);

    return (
        <div className={styles['options']}>
            <ul className={make.className([focus && 'focus'], styles)}>{optionsElmnt}</ul>
        </div>
    );
}
