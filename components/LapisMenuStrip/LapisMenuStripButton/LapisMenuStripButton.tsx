import * as React from 'react';
import setCssVariables from '../../../functions/setCssVariables';

import styles from './LapisMenuStripButton.module.scss';

export interface ILapisMenuStripButtonProps {
    icon?: string;
    color?: string;
    text?: string;

    onClick?: () => any;
}

function LapisMenuStripButton(props: ILapisMenuStripButtonProps) {
    const { onClick } = props;

    const lapisMenuStripButtonRef = React.useRef<HTMLButtonElement>(null);

    const handlerClick = React.useCallback(() => {
        if (onClick) onClick();
    }, [onClick]);

    React.useEffect(() => {
        setCssVariables(lapisMenuStripButtonRef, [
            {
                property: '--lapis-menu-strip-button-background-color',
                value: String(props.color),
            },
        ]);
    }, [props.color]);

    return (
        <button ref={lapisMenuStripButtonRef} className={styles['lapis-menu-strip-button']} onClick={handlerClick}>
            <div>
                <div className={styles['icon']}>{props.icon}</div>
                <div className={styles['text']}>{props.text}</div>
            </div>
        </button>
    );
}

export default React.memo(LapisMenuStripButton);
