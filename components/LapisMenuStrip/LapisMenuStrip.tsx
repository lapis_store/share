import * as React from 'react';
import make from '../../functions/make';
import setCssVariables from '../../functions/setCssVariables';
import TLapisReactElements from '../../types/TLapisReactElements';

import styles from './LapisMenuStrip.module.scss';

export interface ILapisMenuStripProps {
    className?: string;
}

function LapisMenuStrip(props: React.PropsWithChildren<ILapisMenuStripProps>) {
    const lapisMenuStripRef = React.useRef<HTMLDivElement>(null);

    const { children, numberOfChildren } = React.useMemo(() => {
        let numberOfChildren: number = 0;

        const children: TLapisReactElements = React.Children.map(props.children, (child, i) => {
            numberOfChildren = i + 1;
            return (
                <div className='button-wrap' key={i}>
                    {child}
                </div>
            );
        });

        return { children, numberOfChildren };
    }, [props.children]);

    React.useEffect(() => {
        setCssVariables(lapisMenuStripRef, [
            {
                property: '--number-of-children',
                value: String(numberOfChildren),
            },
        ]);
    }, [numberOfChildren]);

    return (
        <div className={make.className([styles['lapis-menu-strip'], props.className])} ref={lapisMenuStripRef}>
            {children}
            {/* <div className={styles['buttons']}>
                <div className={styles['icon']}></div>
                <div className={styles['wrap']}>
                    {children}
                </div>
                
            </div>
            <div className={styles['buttons']}>
                <div className={styles['icon']}></div>
                {children}
            </div>
            <div className={styles['buttons']}>
                <div className={styles['icon']}></div>
                {children}
            </div> */}
        </div>
    );
}

export default React.memo(LapisMenuStrip);
