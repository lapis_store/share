import LapisMenuStrip from './LapisMenuStrip';
import LapisMenuStripButton from './LapisMenuStripButton/LapisMenuStripButton';

export { LapisMenuStripButton };
export default LapisMenuStrip;
