import * as React from 'react';
import make from '../../functions/make';
import useDispatchStop from '../../hooks/useDispatchStop';
import Buttons from './components/Buttons';
import ListImages from './components/ListImages';
import NavigationBar from './components/NavigationBar';

import styles from './ImagesProductSlider.module.scss';

export interface IImagesProductSliderProps {
    className?: string;
    images?: string[];
    alt?: string;
}

function ImagesProductSlider(props: IImagesProductSliderProps) {
    const [positionImageShowing, setPositionImageShowing] = React.useState<number>(0);
    const length: number = props.images?.length || 0;

    const listImageElmntRef = React.useRef<HTMLDivElement>(null);
    const navElmntRef = React.useRef<HTMLDivElement>(null);

    const handlerButtonsClick = React.useCallback((v: number) => {
        if (!navElmntRef.current) return;

        const currentWidth = navElmntRef.current.offsetWidth;

        const currentScrollLeft = navElmntRef.current.scrollLeft;
        const currentPosition = Math.floor(currentScrollLeft / currentWidth);

        navElmntRef.current.scrollLeft = currentWidth * (currentPosition + v);
    }, []);

    const handlerNavigationBarClick = (v: number) => {
        setPositionImageShowing(v);
    };

    const handlerScrollStop = React.useRef(
        useDispatchStop((v: number) => {
            if (!listImageElmntRef.current) return;

            const currentWidth = listImageElmntRef.current.offsetWidth;
            const currentPosition = Math.round(v / currentWidth);

            setPositionImageShowing(currentPosition);
        }, 500),
    );

    const handlerScroll = (e: React.UIEvent<HTMLElement>) => {
        handlerScrollStop.current(e.currentTarget.scrollLeft);
    };

    React.useEffect(() => {
        if (!listImageElmntRef.current) return;

        const currentWidth = listImageElmntRef.current.offsetWidth;

        listImageElmntRef.current.scrollLeft = currentWidth * positionImageShowing;
    }, [positionImageShowing]);

    const isCenter = (props.images?.length || 0) <= 5;

    return (
        <div
            className={make.className([styles['images-product-slider'], isCenter && styles['center'], props.className])}
        >
            <div className={styles['list-images-wrap']} ref={listImageElmntRef} onScroll={handlerScroll}>
                <ListImages images={props.images} alt={props.alt} />
            </div>
            <Buttons onButtonClick={handlerButtonsClick} display={!isCenter} />
            <div className={styles['navigation-bar-wrap']} ref={navElmntRef}>
                <NavigationBar
                    images={props.images}
                    alt={props.alt}
                    highlightPosition={positionImageShowing}
                    onClick={handlerNavigationBarClick}
                />
            </div>
        </div>
    );
}

export default React.memo(ImagesProductSlider);
