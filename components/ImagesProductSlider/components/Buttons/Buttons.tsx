import * as React from 'react';
import make from '../../../../functions/make';

import styles from './Buttons.module.scss';

export interface IButtonsProps {
    className?: string;
    display?: boolean;
    onButtonClick?: (v: 1 | -1) => any;
}

export default function Buttons(props: IButtonsProps) {
    const handlerButtonsClick = (v: 1 | -1) => () => {
        if (props.onButtonClick) props.onButtonClick(v);
    };

    return (
        <div className={make.className([styles['buttons'], !props.display && styles['hide'], props.className])}>
            <div className={styles['wrap']}>
                <button onClick={handlerButtonsClick(-1)}>arrow_left</button>
                <button onClick={handlerButtonsClick(1)}>arrow_right</button>
            </div>
        </div>
    );
}
