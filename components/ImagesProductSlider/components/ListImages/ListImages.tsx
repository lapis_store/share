///* eslint-disable @next/next/no-img-element */
import * as React from 'react';
import make from '../../../../functions/make';
import ListImagesBase from '../ListImagesBase';
import styles from './ListImages.module.scss';

export interface IListImagesProps {
    className?: string;
    images?: string[];
    alt?: string;
}

export default function ListImages(props: IListImagesProps) {
    return (
        <div
            className={make.className([styles['list-images'], props.className])}
            style={
                {
                    '--length': props.images?.length || 0,
                } as any
            }
        >
            <ListImagesBase className={styles['image-wrap']} images={props.images} alt={props.alt} />
        </div>
    );
}
