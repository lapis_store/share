import * as React from 'react';
import make from '../../../../functions/make';
import ListImagesBase from '../ListImagesBase';

import styles from './NavigationBar.module.scss';

export interface INavigationBarProps {
    className?: string;
    images?: string[];
    alt?: string;
    highlightPosition?: number;
    onClick?: (v: number) => any;
}

export default function NavigationBar(props: INavigationBarProps) {
    return (
        <div
            className={make.className([styles['navigation-bar'], props.className])}
            style={
                {
                    '--length': props.images?.length || 0,
                } as any
            }
        >
            <ListImagesBase
                className={styles['image-wrap']}
                highlightClassName={styles['highlight']}
                images={props.images}
                alt={props.alt}
                highlightPosition={props.highlightPosition}
                onClick={props.onClick}
                thumbnail
            />
        </div>
    );
}
