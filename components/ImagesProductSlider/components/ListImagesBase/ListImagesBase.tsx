///* eslint-disable @next/next/no-img-element */
import * as React from 'react';
import make from '../../../../functions/make';

export interface IListImagesBaseProps {
    className?: string;
    highlightClassName?: string;
    highlightPosition?: number;
    images?: string[];
    alt?: string;

    thumbnail?: boolean;

    onClick?: (v: number) => any;
}

export default function ListImagesBase(props: IListImagesBaseProps) {
    const handlerItemOnClick = (v: number) => () => {
        if (props.onClick) props.onClick(v);
    };

    const renderPictureElmnt = (item: string) => {
        if (props.thumbnail) {
            return <img src={make.imageAddress(item, 'thumbnail')} alt={props.alt || ''} />;
        }
        return (
            <picture>
                <source media='(min-width:600px)' srcSet={make.imageAddress(item, 'medium')} />
                <img src={make.imageAddress(item, 'small')} alt={props.alt || ''} />
            </picture>
        );
    };

    const imagesElmnts = props.images?.map((item, i) => {
        return (
            <div
                key={`${item}_${i}`}
                className={make.className([props.className, props.highlightPosition === i && props.highlightClassName])}
                onClick={handlerItemOnClick(i)}
            >
                {renderPictureElmnt(item)}
            </div>
        );
    });
    return <>{imagesElmnts}</>;
}
