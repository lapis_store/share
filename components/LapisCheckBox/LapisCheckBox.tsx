import * as React from 'react';
import make from '../../functions/make';

import './LapisCheckBox.scss';

export interface ILapisCheckBoxProps {
    className?: string;
    title?: string;
    checked?: boolean;
    onChange?: (checked: boolean) => any;
}

export default function LapisCheckBox(props: ILapisCheckBoxProps) {
    const { onChange } = props;

    const inputElmntRef = React.useRef<HTMLInputElement>(null);

    const handlerChange = React.useCallback(
        (e: React.FormEvent<HTMLInputElement>) => {
            // dispatch event
            if (onChange) {
                onChange(e.currentTarget.checked);
            }
        },
        [onChange],
    );

    React.useEffect(() => {
        // avoid checked is false
        if (props.checked === undefined) return;
        //
        if (!inputElmntRef.current) return;
        inputElmntRef.current.checked = props.checked;
    }, [props.checked]);

    return (
        <div className={make.className(['lapis-ui', 'lapis-check-box', props.className])}>
            <input ref={inputElmntRef} type='checkbox' onChange={handlerChange} />

            <div className='title'>{props.title}</div>
        </div>
    );
}
