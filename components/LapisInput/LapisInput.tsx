import * as React from 'react';
import make from '../../functions/make';

import styles from './LapisInput.module.scss';

export interface ILapisInputProps {
    className?: string;

    type?: 'number' | 'text' | 'date' | 'datetime-local' | 'password';
    title?: string;
    defaultValue?: string;
    min?: string | number;
    max?: string | number;
    placeholder?: string;

    multipleLine?: boolean;
    numberOfLines?: number;
    maxHeight?: string;

    // icon
    displayIcon?: boolean;
    helperIcon?: string;
    helperIconColor?: string;
    helperText?: string;

    // button
    displayButton?: boolean;
    buttonIcon?: string;
    buttonIconColor?: string;
    buttonTooltip?: string;

    onChange?: (v: string) => any;
    onBlur?: (v: string) => any;
    onFocus?: (v: string) => any;
    onButtonClick?: () => any;
}

export interface ILapisInputRef {
    value: string;
    focus: () => void;
}

function LapisInput(props: ILapisInputProps, ref: React.RefObject<ILapisInputRef> & any) {
    // console.log('[LapisInput] Rerender');

    const { onChange, onButtonClick } = props;

    const [isEmpty, setEmptyState] = React.useState<boolean>(true);
    const [isTyping, setTypingState] = React.useState<boolean>(false);
    const [inputValue, setInputValue] = React.useState<string>(props.defaultValue || '');

    const inputElmntRef = React.useRef<HTMLInputElement>(null);
    const textAreaElmntRef = React.useRef<HTMLTextAreaElement>(null);

    //
    React.useImperativeHandle(
        ref,
        () =>
            ({
                get value(): string {
                    return inputValue;
                },
                set value(v: string) {
                    setInputValue(v);
                    if (v.length !== 0) setEmptyState(false);
                    if (onChange) onChange(v);
                },
                focus: () => {
                    if (inputElmntRef.current) {
                        inputElmntRef.current.focus();
                    }

                    if (textAreaElmntRef.current) {
                        textAreaElmntRef.current.focus();
                    }
                },
            } as ILapisInputRef),
    );

    // Event handler
    const handlerFocus = (e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const v: string = e.currentTarget.value;
        setTypingState(true);

        if (props.onFocus) props.onFocus(v);
    };

    const handlerBlur = (e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const v = e.currentTarget.value;

        setTypingState(false);
        setEmptyState((preState) => {
            return v.length === 0;
        });

        if (props.onBlur) props.onBlur(v);
    };

    const handlerChange = (e: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const v: string = e.currentTarget.value;
        setInputValue(v);

        if (onChange) onChange(v);

        // if(props.multipleLine && (props.type || 'text') === 'text'){
        //     e.currentTarget.style.height = 5 + 'px';
        //     e.currentTarget.style.height = (e.currentTarget.scrollHeight) + 'px';
        // }
    };

    // React.useEffect(()=>{
    //     if(!props.multipleLine || (props.type || 'text') !== 'text') return;
    //     if(!textAreaElmntRef.current) return;

    //     textAreaElmntRef.current.style.height = 5 + 'px';
    //     textAreaElmntRef.current.style.height = (textAreaElmntRef.current.scrollHeight) + 'px';
    // }, [inputValue, props.multipleLine, props.type]);

    // component dit mount
    const titleElmnt = React.useMemo(
        () => (
            <div
                className={make.className([
                    styles['title'],
                    isTyping && styles['typing'],
                    !isEmpty && styles['not-empty'],
                ])}
            >
                <div>{props.title}</div>
            </div>
        ),
        [props.title, isTyping, isEmpty],
    );

    const iconElmnt = React.useMemo(
        () => (
            <div className={make.className([styles['icon-container'], props.displayIcon && styles['display']])}>
                <div className={styles['icon']} style={{ color: props.helperIconColor }}>
                    {props.helperIcon}
                </div>
                <div className={make.className([styles['helper-text'], props.helperText && styles['displayable']])}>
                    <div>{props.helperText}</div>
                </div>
            </div>
        ),
        [props.helperIconColor, props.helperIcon, props.helperText, props.displayIcon],
    );

    const buttonElmnt = React.useMemo(
        () => (
            <div
                className={make.className([styles['button-container'], props.displayButton && styles['display']])}
                onClick={onButtonClick}
            >
                <div className={styles['icon']} style={{ color: props.buttonIconColor }}>
                    {props.buttonIcon}
                </div>
                <div className={make.className([styles['tooltip'], props.buttonTooltip && styles['displayable']])}>
                    <div>{props.buttonTooltip}</div>
                </div>
            </div>
        ),
        [props.buttonIcon, props.buttonTooltip, props.buttonIconColor, onButtonClick, props.displayButton],
    );

    const inputElmnt = () => {
        let numberOfRightSideSpaces: number = 0;

        if (props.displayButton) numberOfRightSideSpaces += 1;
        if (props.displayIcon) numberOfRightSideSpaces += 1;

        if (props.multipleLine && (props.type || 'text') === 'text') {
            return (
                <textarea
                    className={make.className([styles['input'], styles['textarea'], isEmpty && styles['empty']])}
                    ref={textAreaElmntRef}
                    value={inputValue}
                    onFocus={handlerFocus}
                    onBlur={handlerBlur}
                    onChange={handlerChange}
                    style={
                        {
                            maxHeight: 'calc(100vh - 180px)',
                            '--number-of-lines': props.numberOfLines || 7,
                            '--number-of-right-side-spaces': numberOfRightSideSpaces,
                        } as any
                    }
                />
            );
        }

        return (
            <input
                className={make.className([styles['input'], isEmpty && styles['empty']])}
                ref={inputElmntRef}
                type={props.type || 'text'}
                autoComplete='off'
                value={inputValue}
                min={props.min}
                max={props.max}
                placeholder={props.placeholder}
                onFocus={handlerFocus}
                onBlur={handlerBlur}
                onChange={handlerChange}
                style={
                    {
                        '--number-of-right-side-spaces': numberOfRightSideSpaces,
                    } as any
                }
            />
        );
    };

    const underlineElmnt = React.useMemo(
        () => (
            <div className={make.className([styles['underline'], (isTyping || !isEmpty) && styles['display']])}>
                <div />
            </div>
        ),
        [isTyping, isEmpty],
    );

    return (
        <div
            className={make.className([
                styles['lapis-input'],
                props.className,
                // props.displayIcon && styles['show-icon'],
                // props.displayButton && styles['show-button'],
                // isTyping && styles['typing'],
            ])}
        >
            {titleElmnt}

            <div className={styles['input-wrap']}>
                {inputElmnt()}
                {buttonElmnt}
                {iconElmnt}
            </div>

            {underlineElmnt}
        </div>
    );
}

export default React.memo(React.forwardRef<ILapisInputRef, ILapisInputProps>(LapisInput));
