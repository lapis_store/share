import * as React from 'react';
import LapisLink from '../../../flex/LapisLink';
import make from '../../functions/make';

import styles from './LapisCircularButton.module.scss';

export interface ILapisCircularButtonProps {
    type?: 'button' | 'link' | 'aTag';
    className?: string;
    icon?: string;
    tooltip?: string;
    href?: string;
    openInNewTab?: boolean;
    onClick?: () => any;
}

export default function LapisCircularButton(props: React.PropsWithChildren<ILapisCircularButtonProps>) {
    const iconJsxElmnt = React.useMemo(() => {
        return <div className={styles['icon']}>{props.icon}</div>;
    }, [props.icon]);

    const tooltipJsxElmnt = React.useMemo(() => {
        if (!props.tooltip) return undefined;
        return <div className={styles['tooltip']}>{props.tooltip}</div>;
    }, [props.tooltip]);

    const className = make.className([styles['lapis-circular-button'], props.className]);

    switch (props.type) {
        case 'link': {
            return (
                <LapisLink href={props.href || '/'} className={className} onClick={props.onClick}>
                    {iconJsxElmnt}
                    {tooltipJsxElmnt}
                </LapisLink>
            );
        }
        case 'aTag': {
            return (
                <a
                    href={props.href}
                    className={className}
                    onClick={props.onClick}
                    target={props.openInNewTab ? '_blank' : '_self'}
                    rel='noreferrer'
                >
                    {iconJsxElmnt}
                    {tooltipJsxElmnt}
                </a>
            );
        }
        default: {
            return (
                <button className={className} onClick={props.onClick}>
                    {iconJsxElmnt}
                    {tooltipJsxElmnt}
                </button>
            );
        }
    }
}
