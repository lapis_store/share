import * as React from 'react';
import make from '../../functions/make';

import styles from './LapisCover.module.scss';
export interface ILapisCoverProps {
    display?: boolean;
    className?: string;
    zIndex?: number;
    onClick?: () => any;
    onMouseDown?: () => any;
}

export default function LapisCover(props: React.PropsWithChildren<ILapisCoverProps>) {
    const classDisplay = props.display ? '' : styles['hide'];

    return (
        <div
            className={make.className([styles['lapis-cover'], props.className, classDisplay])}
            style={{
                zIndex: props.zIndex || 0,
            }}
            onClick={props.onClick}
            onMouseDown={props.onMouseDown}
        >
            {props.children}
        </div>
    );
}
