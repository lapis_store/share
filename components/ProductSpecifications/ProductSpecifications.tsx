import * as React from 'react';
import ISpecification from '../../../share_types/others/ISpecification';
import make from '../../functions/make';

import styles from './ProductSpecifications.module.scss';

export interface IProductSpecificationsProps {
    className?: string;
    data?: ISpecification[];
}

export default function ProductSpecifications(props: IProductSpecificationsProps) {
    const data = props.data;
    if (!data) return null;

    const itemsElmnts = data.map((item, i) => {
        return (
            <li key={`${item.name}_${i}`}>
                <div className={styles['name']}>{item.name}</div>
                <div className={styles['value']}>{item.value}</div>
            </li>
        );
    });

    return (
        <div className={make.className([styles['product-specifications'], props.className])}>
            <h2>Thông số sản phẩm</h2>
            <ul>{itemsElmnts}</ul>
        </div>
    );
}
