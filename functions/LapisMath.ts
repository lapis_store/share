export default class LapisMath {
    public static gcd(a: number, b: number): number {
        if (!b) {
            return a;
        }
        return this.gcd(b, a % b);
    }

    public static reduceFraction(a: number, b: number): [number, number] {
        const d = this.gcd(a, b);
        return [Math.floor(a / d), Math.floor(b / d)];
    }
}
