export default class ConvertDate {
    public static toDateTimeLocalValue(strDate?: string) {
        if (!strDate) return '';
        if (strDate.length < 16) return '';

        const d = new Date(strDate);
        const yy = this.formatTimeNumber(d.getFullYear());
        const mm = this.formatTimeNumber(d.getMonth() + 1);
        const dd = this.formatTimeNumber(d.getDate());
        const hours = this.formatTimeNumber(d.getHours());
        const minutes = this.formatTimeNumber(d.getMinutes());

        return `${yy}-${mm}-${dd}T${hours}:${minutes}`;
    }

    public static toStringDateTime(strDate?: string) {
        if (!strDate) return 'undefined';
        if (strDate.length !== 16) return 'undefined';

        const d = new Date(Date.parse(strDate));
        return d.toJSON();
    }

    private static formatTimeNumber(v: number): string {
        if (v < 10) return '0' + String(v);
        return String(v);
    }
}
