import removeVNAccents from './removeVNAccents';

function optimizeKeyword(v: string) {
    return removeVNAccents(v.toLowerCase());
}

export default optimizeKeyword;
