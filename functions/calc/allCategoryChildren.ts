import ICategoryResponse from '../../../share_types/response/admin/category/ICategoryResponse';
import ICategoryRes from '../../../share_types/response/user/category/ICategoryRes';

const allCategoryChildren = async (
    categoryId: string | undefined,
    categories: ICategoryRes[] | ICategoryResponse[],
): Promise<string[]> => {
    if (!categoryId) return [];

    const getCategory = (id: string) => {
        return categories.find((item) => item._id === id);
    };

    const result: Set<string> = new Set();
    let currentId: string | undefined = categoryId;

    while (currentId !== undefined && !result.has(currentId)) {
        const category = getCategory(currentId);

        if (!category) break;

        result.add(category._id);
        currentId = category.parentId;
    }

    return [...result];
};

export default allCategoryChildren;
