import allCategoryChildren from './allCategoryChildren';
import LapisDate from './LapisDate';

const calc = {
    LapisDate,
    allCategoryChildren,
};

export default calc;
