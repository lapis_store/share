export default class LapisDate {
    public static dateToMilliseconds(d: number) {
        // one date = 24*3600*1000 = 86400000 milliseconds
        return d * 86400000;
    }

    public static plus(numberOfDate: number, d: Date = new Date()) {
        const dTime = d.getTime();
        const plusTime = this.dateToMilliseconds(numberOfDate);
        return new Date(dTime + plusTime);
    }
}
