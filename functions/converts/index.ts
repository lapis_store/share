import numericToUnicode from './numericToUnicode';
import dateToInputDatetimeLocalValue from './dateToInputDatetimeLocalValue';
import inputDatetimeLocalValueToDate from './inputDatetimeLocalValueToDate';
import dateToStringLocalDate from './dateToStringLocalDate';

const converts = {
    dateToStringLocalDate,
    numericToUnicode,
    dateToInputDatetimeLocalValue,
    inputDatetimeLocalValueToDate,
};

export default converts;
