import regex from '../../regex';

const convert = (d: Date) => {
    const yy = String(d.getFullYear()).padStart(2, '0');
    const mm = String(d.getMonth() + 1).padStart(2, '0');
    const dd = String(d.getDate()).padStart(2, '0');
    const hours = String(d.getHours()).padStart(2, '0');
    const minutes = String(d.getMinutes()).padStart(2, '0');

    return `${yy}-${mm}-${dd}T${hours}:${minutes}`;
};

const dateToInputDatetimeLocalValue = (d: string | Date = new Date()) => {
    if (typeof d === 'string') {
        if (!regex.jsonDate.test(d)) {
            console.error(`Can't convert '${d}' to Date. Return current time value'`);
            return convert(new Date());
        }
        return convert(new Date(d));
    }

    return convert(d);
};

export default dateToInputDatetimeLocalValue;
