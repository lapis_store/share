const inputDatetimeLocalValueToDate = (strDate?: string) => {
    if (!strDate) return undefined;
    if (strDate.length !== 16) return undefined;

    const result = new Date(Date.parse(strDate));
    return result;
};

export default inputDatetimeLocalValueToDate;
