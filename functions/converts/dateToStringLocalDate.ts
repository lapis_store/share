const dateToStringLocalDate = (v?: string) => {
    if (!v) return '';
    const d = new Date(v);
    const dd = String(d.getDate()).padStart(2, '0');
    const mm = String(d.getMonth() + 1).padStart(2, '0');
    const yyyy = String(d.getFullYear()).padStart(2, '0');
    const h = String(d.getHours()).padStart(2, '0');
    const m = String(d.getMinutes()).padStart(2, '0');
    const s = String(d.getSeconds()).padStart(2, '0');

    return `${dd}/${mm}/${yyyy} ${h}:${m}:${s}`;
};

export default dateToStringLocalDate;
