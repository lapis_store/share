const regexDecChar = /^&#\d+$/i;
const regexHexChar = /^((&#x)|(\\))[0-9a-f]+$/i;

const numericToUnicode = (v: string | number) => {
    if (typeof v === 'number') {
        return String.fromCharCode(v);
    }

    if (regexDecChar.test(v)) {
        // v = '&#0042121541'

        // strNumber = 0042121541
        const vNumeric = parseInt(v.slice(2));

        if (isFinite(vNumeric)) {
            return String.fromCharCode(vNumeric);
        }

        console.log(`[WARNING] Can't convert "${v}" to unicode`);
    }

    if (regexHexChar.test(v)) {
        // v = &#xf2be || \f2be

        const vNumeric = v.startsWith('&') ? parseInt(v.slice(3), 16) : parseInt(v.slice(1), 16);

        if (isFinite(vNumeric)) {
            return String.fromCharCode(vNumeric);
        }
    }

    return v;
};

export default numericToUnicode;
