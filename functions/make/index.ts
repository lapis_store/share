import makeClassName from './className';
import effect from './effect';
import makeId from './id';
import makeImageAddress from './imageAddress';
import linkProduct from './linkProduct';
import makeFullCategoryPath from './makeFullCategoryPath';

const make = {
    fullCategoryPath: makeFullCategoryPath,
    id: makeId,
    effect,
    className: makeClassName,
    imageAddress: makeImageAddress,
    linkProduct,
};

export default make;
