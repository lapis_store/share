import ICategoryResponse from '../../../share_types/response/admin/category/ICategoryResponse';
import ICategoryRes from '../../../share_types/response/user/category/ICategoryRes';

const makeFullCategoryPath = async (
    categoryId: string | undefined,
    categories: ICategoryRes[] | ICategoryResponse[],
): Promise<string> => {
    if (!categoryId) return '';

    const getCategory = (id: string) => {
        return categories.find((item) => item._id === id);
    };

    const getTitle = (id: string, added: Set<string> = new Set()): string => {
        if (added.has(id)) return '';
        added.add(id);

        const category = getCategory(id);
        if (!category) return '';

        if (!category.parentId) return category.title;

        return `${getTitle(category.parentId, added)}/${category.title}`;
    };

    return getTitle(categoryId);
};

export default makeFullCategoryPath;
