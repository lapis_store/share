import ENV_SHARE from '../../core/ENV_SHARE';

type TFileSize = 'large' | 'medium' | 'small' | 'thumbnail';
const linkAddress = /^(http)(s?):\/\/.+$/i;

const makeImageAddress = (imageName: string = '#', fileSize: TFileSize = 'large') => {
    if (!imageName || imageName === '#') return '#';

    if (linkAddress.test(imageName)) {
        return imageName;
    }

    return [ENV_SHARE.STATIC_HOST, 'public', 'images', fileSize, imageName].join('/');
};

export default makeImageAddress;
