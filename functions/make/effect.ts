const effect = (callback: (...props: any) => any) => {
    let preProps: any[] = [];
    return (props: any[]) => {
        if (preProps.length !== props.length) {
            preProps = props;
            callback(...props);
            return;
        }

        for (let i = 0; i < preProps.length; i++) {
            if (preProps[i] !== props[i]) {
                preProps = props;
                callback(...props);
                return;
            }
        }
    };
};

export default effect;
