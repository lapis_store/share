let count = 0;

const generateNumber = () => {
    if (count > 10_000) count = 0;
    else count++;

    return count;
};

const makeId = () => {
    const n = generateNumber().toString(16).padStart(8, '0');
    const time = Date.now().toString(16).padStart(16, '0');
    return time + n;
};

export default makeId;
