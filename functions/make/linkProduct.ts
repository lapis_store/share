const linkProduct = (slug?: string) => {
    if (!slug) return '#';
    return `/products/${slug}`;
};

export default linkProduct;
