function setCssVariables(ref: React.RefObject<HTMLElement>, variables: { property: string; value: string }[]) {
    if (!ref.current) return;

    variables.forEach(({ property, value }) => {
        if (!ref.current) return;
        if (value === 'undefined') return;
        ref.current.style.setProperty(property, value);
    });
}

export default setCssVariables;
