function phoneNumber(v: string) {
    const serviceProvider = v.slice(0, 3);
    const middleNumber = v.slice(3, 6);
    const endNumber = v.slice(6);

    return `${serviceProvider} ${middleNumber} ${endNumber}`;
}

export default phoneNumber;
