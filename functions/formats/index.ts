import phoneNumber from './phoneNumber';

const formats = {
    phoneNumber,
};

export default formats;
