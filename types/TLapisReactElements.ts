type TLapisReactElements = JSX.Element[] | JSX.Element | null | undefined;

export default TLapisReactElements;
