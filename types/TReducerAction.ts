interface TReducerAction<ActionsTypes> {
    type: ActionsTypes;
    payload: any;
}

export default TReducerAction;
